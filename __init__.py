import bpy
import bmesh
import struct
import zlib
import mathutils
import math
import bpy.types
import gongsf
from bpy.props import (
        BoolProperty,
        FloatProperty,
        StringProperty,
        IntProperty,
        EnumProperty,
        )
from bpy_extras.io_utils import (
        ExportHelper,
        axis_conversion,
        )

bl_info = {
    "name": "GenMesh Export Add-on",
    "author": "GlikoPL",
    "blender": (2, 80, 0),
    "location": "File > Import-Export",
    "category": "Import-Export",
}

GMESH_VTX_POS = (1 << 0)
GMESH_VTX_NORMAL_AND_MAT = (1 << 1)

def write_vert(out : bytearray, vert : bpy.types.MeshVertex):
    out += (struct.pack('<f', vert.co.x))
    out += (struct.pack('<f', vert.co.z))
    out += (struct.pack('<f', -vert.co.y))
    
def write_normal_and_mat(out : bytearray, normal, mat_id):
    nx = int(((normal.x + 1.0) / 2.0) * 255)
    ny = int(((normal.z + 1.0) / 2.0) * 255)
    nz = int((((-normal.y) + 1.0) / 2.0) * 255)
    out += ((nx).to_bytes(1, byteorder='little', signed=False))
    out += ((ny).to_bytes(1, byteorder='little', signed=False))
    out += ((nz).to_bytes(1, byteorder='little', signed=False))
    out += ((mat_id).to_bytes(1, byteorder='little', signed=False))

def write_gmesh_material(mat : bpy.types.Material, out : bytearray):
    mat_nodes = mat.node_tree.nodes
    principled = next(n for n in mat_nodes if n.type == 'BSDF_PRINCIPLED')
    if principled is None:
        base_color = mat.diffuse_color
        roughness = mat.roughness
        metallic = mat.metallic
    else:
        base_color = principled.inputs['Base Color']
        base_color_value = base_color.default_value
        roughness = principled.inputs['Roughness'].default_value
        metallic = principled.inputs['Metallic'].default_value
    
    out += struct.pack('<f', roughness)
    out += struct.pack('<f', metallic)
    out += struct.pack('<f', base_color_value[0])
    out += struct.pack('<f', base_color_value[1])
    out += struct.pack('<f', base_color_value[2])
    out += struct.pack('<f', base_color_value[3])
    name_utf8 = mat.name.encode('utf-8')
    out += len(name_utf8).to_bytes(1, byteorder='little', signed=False)
    if len(name_utf8) != 0:
        out += name_utf8

def write_gmesh(context : bpy.types.Context, filepath, report, *, check_existing=True):
    flags = 0
    vtx_attribs_mask = GMESH_VTX_POS | GMESH_VTX_NORMAL_AND_MAT
    
    data = bytearray()
    vtx_data = bytearray()
    idx_data = bytearray()
    
    scene = context.scene
    objects = scene.objects
    
    with open(filepath, "wb") as binary_file:
        binary_file.write(b'\xC7GMH') #GenMesh Magic
        binary_file.write(b'\x01') #GenMesh Version 1
        binary_file.write((flags).to_bytes(1, byteorder='little', signed=False))
        binary_file.write((vtx_attribs_mask).to_bytes(2, byteorder='little', signed=False))
        
        active_objects: set[bpy.types.Object] = set()
        active_materials: set[bpy.types.Material] = set()
        
        depsgraph = context.evaluated_depsgraph_get()
        
        for object in objects:
            if (not object.hide_get()) and (object.type == "MESH"):
                if len(object.to_mesh().polygons) > 0:
                    active_objects.add(object)
                    for mat in object.to_mesh().materials:
                        active_materials.add(mat)
        
        for mat in active_materials:
            write_gmesh_material(mat, data)
        
        binary_file.write(len(active_materials).to_bytes(1, byteorder='little', signed=False))  
        binary_file.write(len(active_objects).to_bytes(1, byteorder='little', signed=False))
        binary_file.write(b'\x00') #Reserved
        binary_file.write(b'\x00') #Reserved
        
        idx_offset = 0
        for object in active_objects:
            object_mesh = object.to_mesh()
            material_indexes = []
            material_ids = []
            bm = bmesh.new()
            bm.from_object(object, depsgraph)
            bmesh.ops.transform(bm, matrix=object.matrix_world, verts=bm.verts)
            bmesh.ops.triangulate(bm, faces=bm.faces[:])
            smooth_shading = object_mesh.polygons[0].use_smooth
            bmesh.ops.recalc_face_normals(bm, faces=bm.faces[:])
            bm.verts.ensure_lookup_table()
            bm.faces.ensure_lookup_table()
            for vert in bm.verts:
                material_indexes.append(0)
            for mat in object_mesh.materials:
                for mat_id, mat2 in enumerate(active_materials):
                    if mat == mat2:
                        material_ids.append(mat_id)
            for face in bm.faces:
                for elem in face.verts:
                    material_indexes[elem.index] = face.material_index
            
            if smooth_shading:
                for face in bm.faces:
                    for elem in face.verts:
                        idx_data += (elem.index + idx_offset).to_bytes(4, byteorder='little', signed=False)
                           
                for vert_index, vert in enumerate(bm.verts):
                    write_vert(vtx_data, vert)
                    write_normal_and_mat(vtx_data, vert.normal, material_ids[material_indexes[vert_index]])
                idx_offset += len(bm.verts)
            else:
                index = 0
                for face in bm.faces:
                    for elem in face.verts:
                        write_vert(vtx_data, elem)
                        write_normal_and_mat(vtx_data, face.normal, material_ids[face.material_index])
                        idx_data += (index + idx_offset).to_bytes(4, byteorder='little', signed=False)
                        index += 1
                idx_offset += index
        
        binary_file.write((len(data)).to_bytes(4, byteorder='little', signed=False)) #Material Data Size
        binary_file.write((len(idx_data)).to_bytes(4, byteorder='little', signed=False)) #Index Data Size
        binary_file.write((len(vtx_data)).to_bytes(4, byteorder='little', signed=False)) #Vertex Data Size

        data += idx_data
        data += vtx_data
        data_zlib = zlib.compress(data)
        
        binary_file.write((len(data_zlib)).to_bytes(4, byteorder='little', signed=False)) #Compressed Data Size
        binary_file.write(data_zlib)
    return {'FINISHED'}

class ExportGMESH(bpy.types.Operator, ExportHelper):
    bl_idname = "export_scene.gmesh"
    bl_label = "Export GenMesh"
    bl_options = {'PRESET'};
    
    filename_ext = ".gmesh"
    filter_glob: StringProperty(
            default="*.gmesh",
            options={'HIDDEN'},
            ) # type: ignore

    def execute(self, context):
        if bpy.ops.object.mode_set.poll():
            bpy.ops.object.mode_set(mode='OBJECT')
        
        keywords = self.as_keywords(ignore=("filepath", "filter_glob"))
        return write_gmesh(context, self.filepath, self.report, **keywords)


def menu_func_export(self, context):
    self.layout.operator(ExportGMESH.bl_idname, text="GenMesh (.gmesh)")

def register():
    print("Hello World")
    bpy.utils.register_class(ExportGMESH);
    bpy.types.TOPBAR_MT_file_export.append(menu_func_export)
    
def unregister():
    bpy.types.TOPBAR_MT_file_export.remove(menu_func_export)
    bpy.utils.unregister_class(ExportGMESH)
    print("Goodbye World")
    
if __name__ == "__main__":
    register()
